*****
Usage
*****

In order to let Tryton know that the invoices should be sent to SII you should
activate the `Send invoices to SII` flag on the fiscalyear form. When this
flag is activated Tryton will create a `SII Record` for each invoice. This
record will be sent automatically to the tax authority by a cron job.

If the record is rejected the error code and description are stored on it.
Tryton will retry to send the record until it is accepted by the tax authority.
